package com.tretyak.twitterapi

class Endpoints {
	
	public static def homeTimeline = "home_timeline.json"
	public static def tweet = "update.json"
	public static def removeTweet = {id -> "destroy/${id}.json"}
}
