package com.tretyak.twitterapi.spec

import com.tretyak.twitterapi.TwitterBaseSpecification
import java.text.SimpleDateFormat
import java.util.regex.Pattern.LastMatch

import com.tretyak.twitterapi.Endpoints
import spock.lang.Specification
import groovyx.net.http.ContentType
import spock.lang.*

class TwitterSpec extends TwitterBaseSpecification {
	
	def setupResponse
	
	def setup() {
		setupResponse = twitter.post(path: Endpoints.tweet, 
				body: [ status: "testTweet"],
				requestContentType: ContentType.URLENC).data
	}
	
	def "Timeline shows newly added tweet"() {
		when:
			def latestTweet = twitter.get(path: Endpoints.homeTimeline).data.first()			
		then: "Date has expected format"
			new SimpleDateFormat("E MMM dd H:m:s z yyyy", Locale.US)
				.parse(latestTweet.created_at)				
		and:
			latestTweet.created_at == setupResponse.created_at
			latestTweet.retweet_count == 0
			latestTweet.text == setupResponse.text			
		cleanup:
			twitter.post(path: Endpoints.removeTweet(setupResponse.id))
	}
	
	def "Can post a tweet"() {
		expect:
			setupResponse.text == "testTweet"			
		cleanup:
			twitter.post(path: Endpoints.removeTweet(setupResponse.id))
	}
	
	def "Can remove a tweet"() {
		when:
			def deleteResponse = twitter.post(path: Endpoints.removeTweet(setupResponse.id)).data
		then:
			deleteResponse == setupResponse
		when:
			def tweets = twitter.get(path: Endpoints.homeTimeline).data
		then:
			!tweets.find { it.id == setupResponse.id }
	}
	
	def "Tweet duplication causes 403 status code"() {
		expect:
			twitter.post(path: Endpoints.tweet, 
				body: [ status: setupResponse.text],
				requestContentType: ContentType.URLENC).status == 403
		cleanup:
			twitter.post(path: Endpoints.removeTweet(setupResponse.id))
	}
}
