package com.tretyak.twitterapi

import groovyx.net.http.RESTClient
import spock.lang.Shared
import spock.lang.Specification

class TwitterBaseSpecification extends Specification {
	
	public static def baseUrl = System.getProperty("test.baseUrl", "https://api.twitter.com/1.1/statuses/")
	static def props = readProperties()
	
	@Shared def twitter = new RESTClient(baseUrl)
	
	def setupSpec() {
		twitter.auth.oauth(props.consumerKey, props.consumerSecret, 
			props.accessToken, props.accessTokenSecret)
	    twitter.handler.failure = twitter.handler.success
	}
	
	static def readProperties() {
		Properties properties = new Properties()
		File propertiesFile = new File(System.getProperty("test.propertiesPath", "auth.properties"))
		propertiesFile.withInputStream {
			properties.load(it)
		}
		return properties	
	}
}
